# qa-reports-pipeline-tests

The goal of this test is to be used as the last step in a pipeline in order to push test results and validate if all suites in a container are triaged.

###### Mandatory Parameters:
- Server: _The server parameters (default port is 8080) and the path (if it applies. The default is /)_
	- ```qe.server.url```
	- ```qe.server.port```
	- ```qe.server.path```


- Authorization: _You need to ask for the ClientId and SecretId of your connector._
	- ```qe.server.clientId```
	- ```qe.server.secretId```
	

- Target: _The targets are the container, executor or product to validate._
		
	- If you need to push a test result you need to run:
		- ```qe.test.views``` i.e.: Deploy,Test,ETC  _(you can set multiple views by separating them with a coma, e.g.: "Deploy App",Develop. The view by default is All)_
		
		Choose the CI that you're using:
		- ```test=JenkinsPushTestSuite```
		
		  or
		- ```test=BambooPushTestSuite```
	
	- If you need to validate the triage result
		- ```qe.test.containerid```
    	- ```qe.test.executorid```
    	- ```qe.test.productid```
		- ```test=com.clarolab.pipeline.test.[PipelineContainerAPITest | PipelineExecutorAPITest | PipelineProductAPITest]```

- Jenkins CI: _The Jenkins server where tests are running._

    - ```qe.jenkins.url```
    - ```qe.jenkins.username```
    - ```qe.jenkins.token```

- Bamboo CI: _The Bamboo server where tests are running._

    - ```qe.bamboo.url```
    - ```qe.bamboo.username```
    - ```qe.bamboo.token```
    
###### Optional Parameters:
- ```qe.test.timeout``` _The amount of milliseconds to wait if the triage is not done. By default it's 30secs_
- ```qe.test.repeat``` _The amount of retries. By default it's 5 times_


**Note:** For more information visit: https://wiki.clarolab.com.ar/community/publico/t-triage-support

_Copyright (c) 2019, Clarolab. All rights reserved._
