package com.clarolab.pipeline;

import com.clarolab.client.JenkinsApiClient;
import com.clarolab.client.JenkinsJobClient;
import com.clarolab.client.JenkinsViewClient;
import com.clarolab.entities.JenkinsBuild;
import com.clarolab.entities.JenkinsContainer;
import com.clarolab.entities.JenkinsJob;
import com.clarolab.pipeline.push.TriageArtifact;
import com.clarolab.pipeline.push.TriageData;
import com.clarolab.pipeline.util.Bitbucket.BitbucketArtifact;
import com.clarolab.pipeline.util.Bitbucket.Pipeline;
import com.clarolab.pipeline.util.DataHelper;
import com.google.common.collect.Lists;
import io.restassured.response.Response;
import lombok.extern.java.Log;
import org.apache.http.HttpStatus;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Level;

import static com.clarolab.pipeline.util.Constants.*;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;

@Log
public class JenkinsAPI {

    //All values belong to jenkins
    private final String url = System.getProperty(QE_JENKINS_URL, "UNKNOWN");
    private final String username = System.getProperty(QE_JENKINS_USERNAME, "UNKNOWN");
    private final String token = System.getProperty(QE_JENKINS_TOKEN, "UNKNOWN");

    public List<TriageData> getAllDataInView(String viewPathOrUrl) {

        List<TriageData> dataList = Lists.newLinkedList();
        try {
            JenkinsApiClient jenkinsApiClient = JenkinsApiClient.builder().userName(username).passwordOrToken(token).baseUrl(url).build();
            JenkinsViewClient jenkinsViewClient = JenkinsViewClient.builder().jenkinsApiClient(jenkinsApiClient).build();
            JenkinsJobClient jenkinsJobClient = JenkinsJobClient.builder().jenkinsApiClient(jenkinsApiClient).build();

            JenkinsContainer container;
            if(!viewPathOrUrl.contains("/"))
                //This case is for single View that lives in jenkins root
                container = jenkinsViewClient.getView(viewPathOrUrl);
            else
                //This case is for nestedView, folder, multiJobs and freeStyle projects w/downstream
                container = jenkinsViewClient.getContainer(viewPathOrUrl);

            List<JenkinsJob> jobs = container.getAllJobs();
            log.info(String.format("All jobs: '%s'. ", jobs.size()));
            jobs.forEach(job -> {
                try {
                    log.info(String.format("Url of the job: '%s'. ", job.getUrl()));
                    JenkinsJob current = jenkinsJobClient.getJob(job.getUrl());
                    JenkinsBuild lastBuild = current.getLastCompletedBuild();
                    if(lastBuild != null){
                        lastBuild.setHttpClient(jenkinsApiClient.getHttpClient());
                        JenkinsBuild bd = lastBuild.getDetails();

                        List<TriageArtifact> artifacts = DataHelper.getArtifactsForJenkins(bd);
                        TriageData triageData = DataHelper.getData(container, job, bd, artifacts);
                        dataList.add(triageData);
                    }else{
                        log.info(String.format("Skipping job '%s' because does not contain builds. It never was executed.", job.getName()));
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, format("Unable to get jobs: %s", e.getMessage()), e);
                }
            });

        } catch (Exception e) {
            log.log(Level.SEVERE, format("Unable to get view '%s': %s", viewPathOrUrl, e.getMessage()), e);
        }

        return dataList;

    }

}
