/*
 * Copyright (c) 2019, Clarolab. All rights reserved.
 */

package com.clarolab.pipeline.util;

import com.clarolab.bamboo.entities.BambooArtifact;
import com.clarolab.bamboo.entities.BambooProject;
import com.clarolab.bamboo.entities.BambooResult;
import com.clarolab.entities.*;
import com.clarolab.pipeline.push.TriageArtifact;
import com.clarolab.pipeline.push.TriageData;
import com.clarolab.pipeline.util.Bitbucket.BitbucketArtifact;
import com.clarolab.pipeline.util.Bitbucket.Pipeline;
import com.google.common.collect.Lists;
import lombok.extern.java.Log;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpEntity;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static com.clarolab.pipeline.util.Constants.ALLOWED_TEST_FILE_EXTENSIONS;
import static com.clarolab.pipeline.util.Constants.QE_SERVER_CLIENT_ID;

@Log
public class DataHelper {

    private static boolean isATestFile(String ext) {
        return Arrays.asList(ALLOWED_TEST_FILE_EXTENSIONS).contains(ext.toLowerCase());
    }

    public static List<TriageArtifact> getArtifactsForJenkins(JenkinsBuild build) {
        List<JenkinsArtifact> artifacts = build.getArtifacts();
        List<TriageArtifact> artifactList = Lists.newArrayList();
        for (JenkinsArtifact artifact : artifacts) {
            String url = getRelativePath(build, artifact);
            String ext = FilenameUtils.getExtension(artifact.getFileName());
            String content = isATestFile(ext) ? getArtifactContent(artifact) : url;
            String fileName = artifact.getFileName();
            artifactList.add(new TriageArtifact(fileName, content, ext, url));
        }
        String logURL = build.getUrl() + "consoleFull";
        artifactList.add(new TriageArtifact("output.log", logURL, "log", logURL));
        return artifactList;
    }

    public static List<TriageArtifact> getArtifactsForBamboo(BambooResult bambooResult) throws AuthenticationException, IOException, URISyntaxException {
        List<BambooArtifact> artifacts = bambooResult.getArtifacts();
        List<TriageArtifact> artifactList = Lists.newArrayList();
        for (BambooArtifact bambooArtifact : artifacts) {
            if(bambooArtifact.isADirectory()){
                log.info("To get elements on a directory as artifact");
                for (Map.Entry<String, String> entry : bambooArtifact.getContentAtDirectory().entrySet()) {
                    artifactList.add(new TriageArtifact(entry.getKey(), entry.getValue(), FilenameUtils.getExtension(entry.getKey()), bambooArtifact.getResourceLink()+entry.getKey()));
                }
                continue;
            }
            String link = bambooArtifact.getResourceLink();
            String fileName = bambooArtifact.getResourceName();
            String ext = bambooArtifact.getResourceExtension();
            String content = isATestFile(ext) ? bambooArtifact.getContent() : link;
            artifactList.add(new TriageArtifact(fileName, content, ext, link));
        }
        return artifactList;
    }

    /**
     * For Bitbucket Pipelines
     * @param pipeline
     * @return
     * @throws AuthenticationException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static List<TriageArtifact> getArtifactsForBitbucketPipelines(Pipeline pipeline, List<BitbucketArtifact> artifacts) {
        List<TriageArtifact> artifactList = Lists.newArrayList();
        artifacts.forEach(a -> {
            String link = ""; //No resource link for this artifact
            String fileName = a.getName();
            String ext = FilenameUtils.getExtension(fileName);
            String content = isATestFile(ext) ? getContentAsJson(a) : link;
            artifactList.add(new TriageArtifact(fileName, content, ext, link));
        });
        return artifactList;
    }

    public static List<TriageArtifact> getArtifactsForGitLabJob(Long jobId, Long projectId, String privateToken) throws IOException {
        List<TriageArtifact> artifactList = new ArrayList<>();

        String apiUrl = String.format(
                "https://gitlab.com/api/v4/projects/%s/jobs/%s/artifacts",
                projectId,
                jobId
        );

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(apiUrl);
            request.addHeader("PRIVATE-TOKEN", privateToken);

            try (CloseableHttpResponse response = httpClient.execute(request)) {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != 200) {
                    throw new IOException("Failed to fetch artifacts from GitLab. Status code: " + statusCode);
                }

                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    // Guardar el archivo ZIP temporalmente
                    Path tempZip = Files.createTempFile("artifacts", ".zip");
                    Files.copy(entity.getContent(), tempZip, StandardCopyOption.REPLACE_EXISTING);

                    // Extraer el contenido del ZIP
                    Path extractDir = Files.createTempDirectory("artifacts_extract");
                    extractZip(tempZip.toFile(), extractDir.toFile());

                    // Leer los archivos extraídos y agregarlos a la lista
                    Files.walk(extractDir).filter(Files::isRegularFile).forEach(file -> {
                        try {
                            String fileName = file.getFileName().toString();
                            String content = FileUtils.readFileToString(file.toFile(), "UTF-8");
                            String ext = fileName.contains(".") ? fileName.substring(fileName.lastIndexOf('.') + 1) : "";
                            artifactList.add(new TriageArtifact(fileName, content, ext, file.toString()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });

                    // Limpiar archivos temporales
                    Files.deleteIfExists(tempZip);
                    FileUtils.deleteDirectory(extractDir.toFile());
                }
            }
        }

        return artifactList;
    }

    private static void extractZip(File zipFile, File outputDir) throws IOException {
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile))) {
            ZipEntry zipEntry;
            while ((zipEntry = zis.getNextEntry()) != null) {
                File newFile = new File(outputDir, zipEntry.getName());
                if (zipEntry.isDirectory()) {
                    newFile.mkdirs();
                } else {
                    newFile.getParentFile().mkdirs();
                    try (FileOutputStream fos = new FileOutputStream(newFile)) {
                        byte[] buffer = new byte[1024];
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer);
                        }
                    }
                }
            }
        }
    }

    private static String downloadArtifactContent(String url, String privateToken) throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(url);
            request.addHeader("PRIVATE-TOKEN", privateToken);

            try (CloseableHttpResponse response = httpClient.execute(request)) {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != 200) {
                    throw new IOException("Failed to download artifact content. Status code: " + statusCode);
                }

                return EntityUtils.toString(response.getEntity());
            }
        }
    }



    private static String getContentAsJson(BitbucketArtifact artifact) {
        String extension = FilenameUtils.getExtension(artifact.getName()).toLowerCase();
        if ("xml".equals(extension)) {
            return XML.toJSONObject(artifact.getContent()).toString(4);
        } else {
            log.warning("Artifact is not a XML file. Returning content as it is. \n Content is: \n" + artifact.getContent());
            return artifact.getContent();
        }
    }

    private static String getRelativePath(JenkinsBuild build, JenkinsArtifact artifact) {
        return build.getUrl() + artifact.get();
    }

    private static String getArtifactContent(JenkinsArtifact artifact) {
        String everything = "";
        try {
            everything = artifact.getContent();
        } catch (Exception e) {
            log.log(Level.WARNING, "There was an error trying to get content for artifact " + artifact.getFileName(), e);
        }
        return everything;
    }

    public static TriageData getData(JenkinsContainer view, JenkinsJob job, JenkinsBuild bd, List<TriageArtifact> artifacts) {
        return new TriageData.Builder()
                .clientId(System.getProperty(QE_SERVER_CLIENT_ID))
                .viewName(view.getName())
                .jobId(0)
                .jobName(job.getName())
                .jobUrl(job.getUrl())
                .buildNumber(bd.getNumber())
                .timestamp(bd.getTimestamp())
                .triggerName("Test")
                .buildStatus(bd.getResult() != null ? bd.getResult() : "UNKNOWN")
                .buildUrl(bd.getUrl())
                .artifacts(artifacts)
                .build();
    }

    public static TriageData getData(BambooProject bambooProject, BambooResult bambooResult, List<TriageArtifact> artifacts) {
        return new TriageData.Builder()
                .clientId(System.getProperty(QE_SERVER_CLIENT_ID))
                .viewName(bambooProject.getName())
                .jobId(0)
                .jobName(bambooResult.getPlanName())
                .buildNumber(bambooResult.getBuildNumber())
                .timestamp(LocalDateTime.parse(bambooResult.getStartTime(), DateTimeFormatter.ISO_DATE_TIME).atZone(ZoneId.systemDefault())
                        .toInstant().toEpochMilli())
                .triggerName("Test")
                .buildStatus(bambooResult.getStatus() != null ? bambooResult.getStatus() : "UNKNOWN")
                .buildUrl(bambooResult.getUrl())
                .artifacts(artifacts)
                .build();
    }

    /**
     * For Bitbucket Pipelines
     * @param pipeline
     * @param artifacts
     * @return
     */
    public static TriageData getData(Pipeline pipeline, List<TriageArtifact> artifacts) {
        String pipelineStatus = "UNKNOWN";
        try {
            pipelineStatus = pipeline.getState().getResult().getName();
        } catch (Exception e) {
            log.warning("Could not get pipeline status. Common case of this event is when the current pipeline is still in progress. If you are trying to get the build status from a pipeline that is in progress, dismiss this warn.");
        }
        return new TriageData.Builder()
                .clientId(System.getProperty(QE_SERVER_CLIENT_ID))
                .viewName(pipeline.getViewName())
                .jobId(0)
                .jobName(pipeline.getJobName() != null ? pipeline.getJobName() : pipeline.getRepository().getName() + pipeline.getBuild_number())
                .buildNumber(pipeline.getBuild_number())
                .timestamp(LocalDateTime.parse(pipeline.getCreated_on(), DateTimeFormatter.ISO_DATE_TIME).atZone(ZoneId.systemDefault())
                        .toInstant().toEpochMilli())
                .triggerName("Test")
//                .buildStatus(pipeline.getState() != null ? pipeline.getState().getResult().getName() : "UNKNOWN")
                .buildStatus(pipelineStatus)
                .buildUrl(pipeline.getWebPipelineUrl())
                .artifacts(artifacts)
                .build();
    }
}
