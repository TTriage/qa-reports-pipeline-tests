package com.clarolab.pipeline.util.Bitbucket;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * User for downloaded artifacts, after unzipping
 */

@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReadArtifact {

    private String name;
    private String content;

}
