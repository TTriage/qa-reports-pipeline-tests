package com.clarolab.pipeline.util.Bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import static com.clarolab.pipeline.util.Constants.QE_BITBUCKET_WORKSPACE;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pipeline {

    private String type;
    private String uuid;
    private long build_number;
    private Account creator;
    private Repository repository;
    private State state;
    private String created_on;
    private String completed_on;

    //USED FOR TTRIAGE
    private String url;
    private String viewName;
    private String jobName;

    private String DEFAULT_BITBUCKET_CLOUD_DOMAIN = "bitbucket.org";


    public String getWebPipelineUrl() {
        // Example: "https://bitbucket.org/MY_WORKSPACE/MY_REPO/pipelines/results/1233"
        return String.format(
                "https://%s/%s/%s/pipelines/results/%d",
                DEFAULT_BITBUCKET_CLOUD_DOMAIN,
                System.getProperty(QE_BITBUCKET_WORKSPACE, "UNKNOWN"),
                this.getRepository().getName(),
                this.build_number
        );
    }

}
