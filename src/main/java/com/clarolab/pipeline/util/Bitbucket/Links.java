package com.clarolab.pipeline.util.Bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Links {

    private Link self;
    private Link html;
    private Link avatar;
    private Link pullrequests;
    private Link commits;
    private Link forks;
    private Link watchers;
    private Link downloads;
    private List<Link> clone;
    private Link hooks;

}
