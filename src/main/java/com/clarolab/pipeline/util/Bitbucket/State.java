package com.clarolab.pipeline.util.Bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class State {

    private String name;
    private String type;
    private Result result;

}
