package com.clarolab.pipeline.util.Bitbucket;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class BitbucketArtifact {

    private String name;
    private String downloadLink = "";
    private String content;

}
