package com.clarolab.pipeline.util.Bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account {

    private String uuid;
    private String username;
    private String display_name;
    private String created_on;
    private Object links;

}
