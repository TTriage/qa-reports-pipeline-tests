package com.clarolab.pipeline.util;

public class Constants {

    public static final String[] ALLOWED_TEST_FILE_EXTENSIONS = {"xml", "json"};

    public static final String QE_JENKINS_URL = "qe.jenkins.url";
    public static final String QE_JENKINS_USERNAME = "qe.jenkins.username";
    public static final String QE_JENKINS_TOKEN = "qe.jenkins.token";

    public static final String QE_BAMBOO_URL = "qe.bamboo.url";
    public static final String QE_BAMBOO_USERNAME = "qe.bamboo.username";
    public static final String QE_BAMBOO_TOKEN = "qe.bamboo.token";

    public static final String QE_BITBUCKET_URL = "qe.bitbucket.url";
    public static final String QE_BITBUCKET_CREDS = "qe.bitbucket.creds";
    public static final String QE_BITBUCKET_USERNAME = "qe.bitbucket.username";
    public static final String QE_BITBUCKET_PASS = "qe.bitbucket.pass";
    public static final String QE_BITBUCKET_WORKSPACE = "qe.bitbucket.workspace";
    public static final String QE_BITBUCKET_REPOSITORY = "qe.bitbucket.repository";
    public static final String QE_BITBUCKET_PIPELINE = "qe.bitbucket.pipeline";
    public static final String QE_BITBUCKET_BUILD_NUMBER = "qe.bitbucket.buildNumber";
    public static final String QE_BITBUCKET_ARTIFACTS = "qe.bitbucket.artifacts";
    public static final String QE_BITBUCKET_VIEW = "qe.bitbucket.view";
    public static final String QE_BITBUCKET_JOB = "qe.bitbucket.job";

    public static final String QE_SERVER_URL = "qe.server.url";
    public static final String QE_SERVER_PORT = "qe.server.port";
    public static final String QE_SERVER_PATH = "qe.server.path";
    public static final String QE_SERVER_CLIENT_ID = "qe.server.clientId";
    public static final String QE_SERVER_SECRET_ID = "qe.server.secretId";

    public static final String QE_TEST_VIEWS = "qe.test.views";
    public static final String QE_TEST_TIMEOUT = "qe.test.timeout";
    public static final String QE_TEST_REPEAT = "qe.test.repeat";

    public static final String QE_GITLAB_URL = "qe.gitlab.url";
    public static final String QE_GITLAB_TOKEN = "qe.gitlab.token";
    public static final String QE_GITLAB_PROJECT_NAME = "qe.gitlab.project.name";


}
