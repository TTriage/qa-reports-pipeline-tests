package com.clarolab.pipeline;

import com.clarolab.bamboo.client.BambooApiClient;
import com.clarolab.bamboo.client.BambooPlanClient;
import com.clarolab.bamboo.client.BambooProjectClient;
import com.clarolab.bamboo.client.BambooResultClient;
import com.clarolab.bamboo.entities.BambooPlan;
import com.clarolab.bamboo.entities.BambooProject;
import com.clarolab.bamboo.entities.BambooResult;
import com.clarolab.pipeline.push.TriageArtifact;
import com.clarolab.pipeline.push.TriageData;
import com.clarolab.pipeline.util.DataHelper;
import com.google.common.collect.Lists;
import joptsimple.internal.Strings;
import lombok.extern.java.Log;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.logging.Level;

import static com.clarolab.pipeline.util.Constants.*;
import static java.lang.String.format;


@Log
public class BambooAPI {

    //All values belong to bamboo
    private final String UNKNOWN = "UNKNOWN";
    private final String url = System.getProperty(QE_BAMBOO_URL, UNKNOWN);
    private String username = System.getProperty(QE_BAMBOO_USERNAME, UNKNOWN);
    private String token = System.getProperty(QE_BAMBOO_TOKEN, UNKNOWN);

    public List<TriageData> getAllDataInView(String viewName) {

        List<TriageData> dataList = Lists.newLinkedList();

        try {
            if(isGuestEnabled()){
                username = Strings.EMPTY;
                token = Strings.EMPTY;
            }
            BambooApiClient bambooApiClient = new BambooApiClient(url, username, token);
            BambooProject bambooProject = new BambooProjectClient(bambooApiClient).getProjectFromName(viewName);
            List<BambooPlan> bambooPlans = new BambooPlanClient(bambooApiClient).getPlansForProject(viewName);

            for (BambooPlan bambooPlan : bambooPlans) {
                //Latest Build of the BambooPlan
                BambooResult bambooResult;
                List<BambooResult> bambooResults = new BambooResultClient(bambooApiClient, 1).getResultsForPlanFromStages(bambooPlan.getPlanKey());
                if(CollectionUtils.isEmpty(bambooResults))
                    continue;
                else
                    bambooResult = bambooResults.get(0);
                List<TriageArtifact> artifacts = DataHelper.getArtifactsForBamboo(bambooResult);
                TriageData triageData = DataHelper.getData(bambooProject, bambooResult, artifacts);
                dataList.add(triageData);
            }

        } catch (Exception e) {
            log.log(Level.SEVERE, format("There was a problem processing project '%s': [%s]", viewName, e.getMessage()), e);
        }

        return dataList;

    }

    private boolean isGuestEnabled(){
        return username.equals(UNKNOWN) && token.equals(UNKNOWN);
    }

}
