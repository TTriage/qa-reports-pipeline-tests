/*
 * Copyright (c) 2019, Clarolab. All rights reserved.
 */

package com.clarolab.pipeline.push;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TriageArtifact {

    private String fileName;
    private String content;
    private String fileType;
    private String url;

    public TriageArtifact() {
    }

    public TriageArtifact(String fileName, String content, String fileType, String url) {
        this.fileName = fileName;
        this.content = content;
        this.fileType = fileType;
        this.url = url;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getUrl(){
        return this.url;
    }
}
