/*
 * Copyright (c) 2019, Clarolab. All rights reserved.
 */

package com.clarolab.pipeline.push;


import java.util.List;

public class TriageData {

    private String viewName;
    private long jobId;
    private String jobName;
    private String jobUrl;
    private long buildNumber;
    private String buildStatus;
    private String buildUrl;
    private List<TriageArtifact> artifacts;

    private long timestamp;
    private String triggerName;

    private String clientId;

    private TriageData() {
    }

    public static class Builder {

        private final TriageData data = new TriageData();

        public TriageData build(){
            return data;
        }

        public Builder viewName(String viewName) {
            data.setViewName(viewName);
            return this;
        }

        public Builder jobId(long jobId) {
            data.setJobId(jobId);
            return this;
        }

        public Builder jobName(String jobName) {
            data.setJobName(jobName);
            return this;
        }

        public Builder jobUrl(String jobUrl) {
            data.setJobUrl(jobUrl);
            return this;
        }

        public Builder buildNumber(long buildNumber) {
            data.setBuildNumber(buildNumber);
            return this;
        }

        public Builder buildStatus(String buildStatus) {
            data.setBuildStatus(buildStatus);
            return this;
        }

        public Builder buildUrl(String buildUrl) {
            data.setBuildUrl(buildUrl);
            return this;
        }

        public Builder artifacts(List<TriageArtifact> artifacts) {
            data.setArtifacts(artifacts);
            return this;
        }

        public Builder timestamp(long timestamp) {
            data.setTimestamp(timestamp);
            return this;
        }

        public Builder triggerName(String triggerName) {
            data.setTriggerName(triggerName);
            return this;
        }

        public Builder clientId(String clientId) {
            data.setClientId(clientId);
            return this;
        }
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobUrl() {
        return jobUrl;
    }

    public void setJobUrl(String jobUrl) {
        this.jobUrl = jobUrl;
    }

    public long getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(long buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getBuildStatus() {
        return buildStatus;
    }

    public void setBuildStatus(String buildStatus) {
        this.buildStatus = buildStatus;
    }

    public String getBuildUrl() {
        return buildUrl;
    }

    public void setBuildUrl(String buildUrl) {
        this.buildUrl = buildUrl;
    }

    public List<TriageArtifact> getArtifacts() {
        return artifacts;
    }

    public void setArtifacts(List<TriageArtifact> artifacts) {
        this.artifacts = artifacts;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
