package com.clarolab.pipeline;

import com.clarolab.pipeline.push.TriageArtifact;
import com.clarolab.pipeline.push.TriageData;
import com.clarolab.pipeline.util.Bitbucket.BitbucketArtifact;
import com.clarolab.pipeline.util.Bitbucket.Pipeline;
import com.clarolab.pipeline.util.DataHelper;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import lombok.extern.java.Log;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

import java.io.*;
import java.net.URLEncoder;
import java.util.*;

import static com.clarolab.pipeline.util.Constants.*;
import static io.restassured.RestAssured.given;


@Log
public class BitbucketPipelineAPI {

    //All values belong to bitbucket
    private final String UNKNOWN = "UNKNOWN";
    private final String url = System.getProperty(QE_BITBUCKET_URL, UNKNOWN);
    private String username = System.getProperty(QE_BITBUCKET_USERNAME, UNKNOWN);
    private String pass = System.getProperty(QE_BITBUCKET_PASS, UNKNOWN);
    private String creds = System.getProperty(QE_BITBUCKET_CREDS, UNKNOWN);
    private String workspace = System.getProperty(QE_BITBUCKET_WORKSPACE, UNKNOWN);
    private String repo = System.getProperty(QE_BITBUCKET_REPOSITORY, UNKNOWN);
    private String pipeline = System.getProperty(QE_BITBUCKET_PIPELINE, UNKNOWN);
    private String artifacts = System.getProperty(QE_BITBUCKET_ARTIFACTS, UNKNOWN);
    private String view = System.getProperty(QE_BITBUCKET_VIEW, "Unclassified Bitbucket pipelines");
    private String job = System.getProperty(QE_BITBUCKET_JOB, null);

    //headers
    private Headers headers = new Headers(new ArrayList<>());

    private final String BASE_URI = "https://api.bitbucket.org";
    private final String PATH_WORKSPACE = "/repositories/{workspace}" ;
    private final String PATH_PIPELINES = PATH_WORKSPACE + "/{repo}/pipelines/";
    private final String PATH_PIPELINES_RECENT_FIRST = PATH_WORKSPACE + "/{repo}/pipelines/?sort=-created_on";
    private final String PATH_PIPELINE = PATH_WORKSPACE + "/{repo}/pipelines/{pipeline}";
    private final String PATH_DOWNLOAD = PATH_WORKSPACE + "/repo/downloads/";

    //Flag to determinate if authentication should be performed or not
    private boolean guests_enabled = false;

    private void restAssuredInit() {
        RestAssured.reset();
        RestAssured.baseURI = BASE_URI;
        RestAssured.port = Integer.parseInt("443");
        RestAssured.basePath = "/2.0";
        RestAssured.defaultParser = Parser.JSON;
    }

    /**
     * Prepare request parameters
     * @throws Exception
     */
    public void prepare() throws Exception {
        restAssuredInit();
        String credentials = null;
        if (!UNKNOWN.equals(creds)) {
            credentials = creds;
        } else if (!UNKNOWN.equals(username) && !UNKNOWN.equals(pass)) {
            credentials = username + ":" + pass;
        } else {
            guests_enabled = true;
            log.warning("Data required for authentication missing. Assuming that anonymous access is granted.");
        }

        if (!guests_enabled) {
            headers = new Headers(new ArrayList<>(Arrays.asList(
                    new Header("Authorization", "Basic " + Base64.getEncoder().encodeToString((credentials).getBytes("UTF-8")))
            )));
        }
    }

    /**
     * Request the pipeline data from Bitbucket
     * @return
     * @throws UnsupportedEncodingException
     */
    private Pipeline getPipeline() throws UnsupportedEncodingException {
        Response response = given()
                .urlEncodingEnabled(false) //It's required to avoid encoding curly braces that are path params
                .pathParam("workspace", URLEncoder.encode(workspace, "UTF-8"))
                .pathParam("repo", URLEncoder.encode(repo, "UTF-8"))
                .pathParam("pipeline", URLEncoder.encode(pipeline, "UTF-8"))
                .headers(headers)
                .log().all()
                .when()
                .get(PATH_PIPELINE);
        response.then().log().all();
        response.then().assertThat().statusCode(HttpStatus.SC_OK);

        Pipeline p = response.as(Pipeline.class);

        if (!url.equals(UNKNOWN)) {
            p.setUrl((url + PATH_PIPELINE).replace("//", "/"));
        } else {
            try {
                p.setUrl(p.getRepository().getLinks().getSelf().getHref());
            } catch (Exception e) {
                log.warning("Pipeline URL could not be found. Leave it in blank.");
                e.printStackTrace();
                p.setUrl("");
            }
        }

        p.setViewName(view);
        p.setJobName(job);
        return p;
    }

    /**
     * Read the content of a file and return it as a string
     * @param file
     * @return
     */
    public String readFileContentAndReturnContentAsString(String file) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            log.warning("Unable to read artifact '" + file +  "' from pipeline source. Make sure it exists.");
            e.printStackTrace();
        }
        try {
            return IOUtils.toString(fis, "UTF-8");
        } catch (IOException e) {
            log.severe("There was an error trying to read artifact '" + file +  "' from pipeline source.");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Iterate across the artifact list and build a list of that in a class that can be processed later
     * @param artifactsList
     * @return
     */
    private List<BitbucketArtifact> readArtifacts(String artifactsList) {
        if (StringUtils.isEmpty(artifactsList)) {
            log.severe("No artifacts passed as parameters. Method with exit with null.");
            return null;
        } else {
            String parts[] = artifactsList.split(",");

            log.info("Artifacts received " + parts.length + ". They are listed right below:");
            Arrays.stream(parts).forEach(p -> log.info("    - Artifact name:" + p));

            if (parts.length < 1) {
                log.severe("No artifacts passed by parameter. Method with exit with null.");
                return null;
            } else {
                List<BitbucketArtifact> artifacts = new ArrayList<>();
                Arrays.stream(parts).forEach(p -> {
                    String pipelineDirectory = new File(new File("").getAbsolutePath()).getParent() + "/"; //Need to get the current parent directory that belongs to the actual pipeline

                    log.info("File " + p + " has extension " + FilenameUtils.getExtension(p) + ".");
                    if (StringUtils.isEmpty(FilenameUtils.getExtension(p))) {
                        //Flow jumps to in here if the current value does not have an extension. It means it's a folder.

                        File folder = new File(pipelineDirectory + p);
                        File[] listOfFiles = folder.listFiles();

                        log.info("Folder artifact found with " + listOfFiles.length + " items in it. Folder location is " + folder.getAbsolutePath());
                        Arrays.stream(listOfFiles).forEach(e -> log.info("    Item: " + e));

                        for (int i = 0; i < listOfFiles.length; i++) {
                            //TODO: shows check if the extension belongs to a readable file
                            String fileLocation = folder.getAbsolutePath() + "/" + listOfFiles[i].getName();
                            log.info("Artifact file location " + fileLocation);
                            String content = readFileContentAndReturnContentAsString(fileLocation);
                            if (content != null) {
                                log.info("      File content is: " + content);
                                artifacts.add(
                                        BitbucketArtifact
                                                .builder()
                                                .name(listOfFiles[i].getName())
                                                .content(content)
                                                .build()
                                );
                                log.info("      Added!");
                            }
                        }
                    } else {
                        //Flow jumps here if the current artifact has an extension
                        //TODO: shows check if the extension belongs to a readable file
                        String fileLocation = pipelineDirectory + p;
                        log.info("Artifact file location " + fileLocation);
                        String content = readFileContentAndReturnContentAsString(fileLocation);
                        if (content != null) {
                            log.info("      File content is: " + content);
                            artifacts.add(
                                    BitbucketArtifact
                                            .builder()
                                            .name(p)
                                            .content(content)
                                            .build()
                            );
                            log.info("      Added!");
                        }
                    }
                    log.info("      Next iteration.");
                });
                log.info("Artifacts set size is " + artifacts.size() + ".");
                return artifacts;
            }
        }
    }

    /**
     * Build pipeline data and push to ttriage
     * @return
     * @throws UnsupportedEncodingException
     */
    public TriageData getPipelineData() throws UnsupportedEncodingException {
        Pipeline pipeline = getPipeline();
        List<BitbucketArtifact> readArtifacts = readArtifacts(artifacts);

        List<TriageArtifact> triageArtifacts = DataHelper.getArtifactsForBitbucketPipelines(pipeline, readArtifacts);

        TriageData triageData = DataHelper.getData(pipeline, triageArtifacts);
        triageData.setClientId(System.getProperty(QE_SERVER_CLIENT_ID));
        return triageData;
    }

}
