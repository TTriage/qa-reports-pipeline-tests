package com.clarolab.pipeline;

import com.clarolab.pipeline.util.DataHelper;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.PipelineApi;
import org.gitlab4j.api.JobApi;
import org.gitlab4j.api.models.Job;
import org.gitlab4j.api.models.Pipeline;
import org.gitlab4j.api.models.Project;
import com.clarolab.pipeline.push.TriageData;
import lombok.extern.java.Log;
import org.gitlab4j.api.models.ProjectFilter;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import static com.clarolab.pipeline.util.Constants.QE_GITLAB_TOKEN;
import static com.clarolab.pipeline.util.Constants.QE_GITLAB_URL;
import static com.clarolab.pipeline.util.Constants.QE_SERVER_CLIENT_ID;

@Log
public class GitLabAPI {

    private final String gitlabUrl = System.getProperty(QE_GITLAB_URL, "https://gitlab.com");
    private final String personalAccessToken = System.getProperty(QE_GITLAB_TOKEN, "UNKNOWN");

    private final GitLabApi gitLabApi;

    public GitLabAPI() {
        this.gitLabApi = new GitLabApi(gitlabUrl, personalAccessToken);
    }

    public List<TriageData> getAllDataInProject(String projectName) {
        List<TriageData> triageDataList = new ArrayList<>();

        try {
            ProjectFilter filter = new ProjectFilter().withSearch(projectName);
            List<Project> projects = gitLabApi.getProjectApi().getProjects(filter);

            if (projects.isEmpty()) {
                log.warning("No projects found with name: " + projectName);
                return triageDataList;
            }

            Project project = projects.get(0);
            log.info("Processing project: " + project.getName());

            PipelineApi pipelineApi = gitLabApi.getPipelineApi();
            JobApi jobApi = gitLabApi.getJobApi();
            List<Pipeline> pipelines = pipelineApi.getPipelines(project.getId());

            for (Pipeline pipeline : pipelines) {
                log.info("Processing pipeline: " + pipeline.getId() + " - Status: " + pipeline.getStatus());

                try {
                    List<Job> jobs = jobApi.getJobsForPipeline(project.getId(), pipeline.getId());

                    for (Job job : jobs) {
                        log.info("Processing job: " + job.getName() + " - Status: " + job.getStatus());

                        TriageData triageData = new TriageData.Builder()
                                .viewName(project.getPathWithNamespace())
                                .jobId(0)
                                .jobName(job.getName())
                                .jobUrl(job.getWebUrl())
                                .buildNumber(job.getPipeline().getId())
                                .buildStatus(job.getStatus().toString())
                                .buildUrl(job.getWebUrl())
                                .timestamp(System.currentTimeMillis())
                                .triggerName(job.getStage())
                                .clientId(System.getProperty(QE_SERVER_CLIENT_ID))
                                .artifacts(DataHelper.getArtifactsForGitLabJob(job.getId(), project.getId(), personalAccessToken))
                                .build();

                        triageDataList.add(triageData);
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Error processing pipeline: " + pipeline.getId(), e);
                }
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "Error getting GitLab project", e);
        }

        return triageDataList;
    }


}
