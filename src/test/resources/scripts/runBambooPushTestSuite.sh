#!/bin/bash

mvn clean test -Dtest=BambooPushTestSuite -Dqe.server.url=http://ttriageback.clarolab.com -Dqe.server.port=80 -Dqe.server.clientId=tVFpYZkPxpezz0XHJOukRbYP@ZOT5sTT.i  -Dqe.server.secretId=otTuto7tmvixvv4yuleaMJu0M3Z5RFqZJsGj4RbcG1sSqKAIL4TZPj6U9IoD3z8Z -Dqe.test.views="Integration Test Suites" -Dqe.bamboo.url=http://dev.clarolab.com:8085 -Dqe.bamboo.username=bamboo -Dqe.bamboo.token=bamboo123

# check status
STATUS=$?

echo "Checking out test report directory"
echo "**********************************"
find ~/repo/target/surefire-reports -type f -printf "%f\n"
echo

echo "Performing file change name to junit-results.xml"
cp ~/repo/target/surefire-reports/TEST-*.xml ~/repo/target/surefire-reports/junit-results.xml
echo

echo "Checking out test report directory"
echo "**********************************"
find ~/repo/target/surefire-reports -type f -printf "%f\n"
echo

echo "exit $STATUS"
exit $STATUS