#!/bin/bash

mvn clean test -Dtest=JenkinsPushTestSuite -Dqe.server.url=http://ttriageback.clarolab.com -Dqe.server.port=80 -Dqe.server.clientId=7UIErxMoghOgYmCJDAqtKSOW@f4WZlS7.i  -Dqe.server.secretId=2SnSFtriWwy1DJynMs4vym283xz4GVOf5gFpbVDLVnOVBE3cxbf4DvrZB1DBeTaW -Dqe.test.views=Application,"Application Push" -Dqe.jenkins.url=http://dev.clarolab.com:12080 -Dqe.jenkins.username=admin -Dqe.jenkins.token=115977a708eaccbfcc825e86ba0a368fb9

# check status
STATUS=$?

echo "Checking out test report directory"
echo "**********************************"
find ~/repo/target/surefire-reports -type f -printf "%f\n"
echo

echo "Performing file change name to junit-results.xml"
cp ~/repo/target/surefire-reports/TEST-*.xml ~/repo/target/surefire-reports/junit-results.xml
echo

echo "Checking out test report directory"
echo "**********************************"
find ~/repo/target/surefire-reports -type f -printf "%f\n"
echo

echo "exit $STATUS"
exit $STATUS