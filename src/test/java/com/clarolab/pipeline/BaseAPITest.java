package com.clarolab.pipeline;

import com.clarolab.pipeline.auth.Authentication;
import com.clarolab.pipeline.auth.Credentials;
import com.clarolab.pipeline.push.TriageData;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import lombok.extern.java.Log;
import org.junit.Before;

import java.util.List;
import java.util.logging.Level;

import static com.clarolab.pipeline.util.Constants.*;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertNotNull;

@Log
public abstract class BaseAPITest {


    private static final String AUTH ="/auth";
    private static final String TOKEN ="/token";

    private static int timeOut = 30000;
    private static int repeats = 5;
    private static int maxRetries = 5;
    private static long waitTime = 1000*3;

    protected static final Long longTimeOut = 5000L;
    protected static final String API_RELEASE_STATUS_URI = "/status";
    protected static final String API_PUSH_URI = "/v1/build/push";

    private Authentication authentication;

    private static boolean initialized;

    @Before
    public void setUp() {
        if (!initialized)
            initialize();
    }

    private void initialize() {
        RestAssured.baseURI = System.getProperty(QE_SERVER_URL, "http://localhost");
        RestAssured.port = Integer.parseInt(System.getProperty(QE_SERVER_PORT, "8080"));
        RestAssured.basePath = System.getProperty(QE_SERVER_PATH, "/");
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        if(RestAssured.baseURI.startsWith("https"))
            RestAssured.useRelaxedHTTPSValidation();

        timeOut = Integer.getInteger(System.getProperty(QE_TEST_TIMEOUT), timeOut);
        repeats = Integer.getInteger(System.getProperty(QE_TEST_REPEAT), repeats);

        authenticate();

        //default settings
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON)
                .addHeader("Authorization", "Bearer "+ authentication.getAccessToken())
                .addFilter(new RequestLoggingFilter())   //to log the complete request
                .addFilter(new ResponseLoggingFilter())  //to log the complete response
                .build();

        initialized = true;
    }

    private void authenticate() {
        Credentials credentials = new Credentials();
        credentials.setClientId(System.getProperty(QE_SERVER_CLIENT_ID));
        credentials.setSecretId(System.getProperty(QE_SERVER_SECRET_ID));

        authentication =
                given()
                        .accept(ContentType.JSON)
                        .contentType(ContentType.JSON)
                        .body(credentials)
                        .expect()
                        .statusCode(200)
                        .when()
                        .post(AUTH + TOKEN)
                        .body().as(Authentication.class);

        assertNotNull(authentication);
        assertNotNull(authentication.getAccessToken());
    }

    public static int getTimeOut() {
        return timeOut;
    }

    public static int getRepeats() {
        return repeats;
    }

    protected Boolean getResult(String id, String path) {
        Boolean result;
        int i = 1;
        do {
            //GET
            result = given()
                    .get(API_RELEASE_STATUS_URI + path + "/" + id)
                    .then()
                    .statusCode(200)
                    .time(lessThan(longTimeOut)).extract().as(Boolean.class);
            log.info(format("Attempt %s of %s is %s", i, getRepeats(), result));

            if (!result) {
                try {
                    Thread.sleep(getTimeOut());
                } catch (InterruptedException e) {
                    log.severe(e.getMessage());
                }
            }

            i++;
        } while (!result && i <= getRepeats());
        return result;
    }

    protected void pushData(TriageData triageData){
        for(int i = 0; i<maxRetries; i++){
            try {
                singlePushData(triageData);
                break;
            }catch(Exception e){
                try {
                    log.log(Level.WARNING, String.format("Error trying to push data.\nReason: %s\nWaiting and trying again. ", e.getMessage()), e);
                    Thread.sleep(waitTime);
                } catch (InterruptedException ie) {
                }
            }
        }
    }

    private void singlePushData(TriageData triageData){
        given()
                .body(triageData)
                .contentType(ContentType.JSON)
                .post(API_PUSH_URI);
    }

    protected void pushData(List<TriageData> triageDataList) {
        int size = triageDataList.size();
        log.info(format("Pushing %s jobs", size));
        int i = 0;

        RestAssured.reset();
        initialize();
        for (TriageData triageData : triageDataList) {
            pushData(triageData);
            i = i + 1;
            log.info(format("Pushed %s of %s", i, size));
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException e) {

            }
        }
    }

}
