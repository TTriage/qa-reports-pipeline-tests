package com.clarolab.pipeline.test.validation;

import com.clarolab.pipeline.BaseAPITest;
import org.junit.Assert;
import org.junit.Test;

public class PipelineExecutorAPITest extends BaseAPITest {

    private static final String EXECUTOR = "/executor";
    public static final String QE_TEST_EXECUTORID = "qe.test.executorid";

    @Test
    public void testTriageIsSuccessInProduct() {
        String id = System.getProperty(QE_TEST_EXECUTORID, "0");
        Boolean result = getResult(id, EXECUTOR);
        Assert.assertTrue("The result should be true", result);
    }


}
