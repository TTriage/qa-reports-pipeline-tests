package com.clarolab.pipeline.test.validation;

import com.clarolab.pipeline.BaseAPITest;
import org.junit.Assert;
import org.junit.Test;

public class PipelineContainerAPITest extends BaseAPITest {

    private static final String CONTAINER = "/container";
    public static final String QE_TEST_CONTAINERID = "qe.test.containerid";

    @Test
    public void testTriageIsSuccessInContainer() {
        String id = System.getProperty(QE_TEST_CONTAINERID, "0");
        Boolean result = getResult(id, CONTAINER);
        Assert.assertTrue("The result should be true", result);
    }

}
