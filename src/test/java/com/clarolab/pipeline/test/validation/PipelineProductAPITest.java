package com.clarolab.pipeline.test.validation;

import com.clarolab.pipeline.BaseAPITest;
import org.junit.Assert;
import org.junit.Test;

public class PipelineProductAPITest extends BaseAPITest {

    private static final String PRODUCT = "/product";
    public static final String QE_TEST_PRODUCTID = "qe.test.productid";

    @Test
    public void testTriageIsSuccessInProduct() {
        String id = System.getProperty(QE_TEST_PRODUCTID, "0");
        Boolean result = getResult(id, PRODUCT);
        Assert.assertTrue("The result should be true", result);
    }
}
