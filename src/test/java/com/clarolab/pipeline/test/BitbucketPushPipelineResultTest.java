package com.clarolab.pipeline.test;

import com.clarolab.pipeline.BaseAPITest;
import com.clarolab.pipeline.BitbucketPipelineAPI;
import com.clarolab.pipeline.push.TriageData;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class BitbucketPushPipelineResultTest extends BaseAPITest {

    @Test
    public void getPipelineResults() throws Exception {
        BitbucketPipelineAPI bitbucketPipelineAPI = new BitbucketPipelineAPI();
        bitbucketPipelineAPI.prepare();

        List<TriageData> allDataInView = new ArrayList<>();
        allDataInView.add(bitbucketPipelineAPI.getPipelineData());
        pushData(allDataInView);
    }
}
