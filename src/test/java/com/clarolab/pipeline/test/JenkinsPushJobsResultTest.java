package com.clarolab.pipeline.test;

import com.clarolab.pipeline.BaseAPITest;
import com.clarolab.pipeline.JenkinsAPI;
import com.clarolab.pipeline.push.TriageData;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.clarolab.pipeline.util.Constants.QE_TEST_VIEWS;

public class JenkinsPushJobsResultTest extends BaseAPITest {

    @Test
    public void testPushJobsViewResultTest() {
        JenkinsAPI jenkinsAPI = new JenkinsAPI();

        String views = System.getProperty(QE_TEST_VIEWS, "UNKNOWN");
        List<String> viewList = Arrays.asList(views.split(","));

        viewList.forEach(view -> {
            List<TriageData> allDataInView = jenkinsAPI.getAllDataInView(view);
            pushData(allDataInView);

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println("The push fails");
            }

        });
    }
}
