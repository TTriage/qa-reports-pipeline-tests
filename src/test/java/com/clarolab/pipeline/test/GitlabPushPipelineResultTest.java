package com.clarolab.pipeline.test;

import com.clarolab.pipeline.BaseAPITest;
import com.clarolab.pipeline.GitLabAPI;
import com.clarolab.pipeline.JenkinsAPI;
import com.clarolab.pipeline.push.TriageData;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.clarolab.pipeline.util.Constants.QE_GITLAB_PROJECT_NAME;
import static com.clarolab.pipeline.util.Constants.QE_TEST_VIEWS;

public class GitlabPushPipelineResultTest extends BaseAPITest {

    @Test
    public void testPushJobsViewResultTest() {
        GitLabAPI gitLabAPI = new GitLabAPI();

        String project = System.getProperty(QE_GITLAB_PROJECT_NAME, "UNKNOWN");
        List<TriageData> allDataInProject = gitLabAPI.getAllDataInProject(project);
        pushData(allDataInProject);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            System.out.println("The push fails");
        }
    }
}

