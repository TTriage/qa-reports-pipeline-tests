package com.clarolab.pipeline.test;

import com.clarolab.pipeline.BambooAPI;
import com.clarolab.pipeline.BaseAPITest;
import com.clarolab.pipeline.push.TriageData;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.clarolab.pipeline.util.Constants.QE_TEST_VIEWS;

public class BambooPushJobsResultTest extends BaseAPITest {

    @Test
    public void exampleTest() {
        BambooAPI bambooAPI = new BambooAPI();

        String views = System.getProperty(QE_TEST_VIEWS, "UNKNOWN").replace("\"", "");
        List<String> viewList = Arrays.asList(views.split(","));

        viewList.forEach(view -> {
            List<TriageData> allDataInView = bambooAPI.getAllDataInView(view);
            pushData(allDataInView);

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {

            }

        });

    }

}
