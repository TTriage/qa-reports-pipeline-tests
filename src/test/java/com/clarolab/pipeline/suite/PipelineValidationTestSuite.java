package com.clarolab.pipeline.suite;

import com.clarolab.pipeline.test.BitbucketPushPipelineResultTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        BitbucketPushPipelineResultTest.class
})
public class PipelineValidationTestSuite {
}
