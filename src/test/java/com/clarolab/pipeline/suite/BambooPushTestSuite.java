package com.clarolab.pipeline.suite;

import com.clarolab.pipeline.test.BambooPushJobsResultTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        BambooPushJobsResultTest.class
})
public class BambooPushTestSuite {
}
