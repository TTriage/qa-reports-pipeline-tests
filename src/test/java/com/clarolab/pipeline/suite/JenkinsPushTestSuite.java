package com.clarolab.pipeline.suite;

import com.clarolab.pipeline.test.JenkinsPushJobsResultTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        JenkinsPushJobsResultTest.class
})
public class JenkinsPushTestSuite {
}
