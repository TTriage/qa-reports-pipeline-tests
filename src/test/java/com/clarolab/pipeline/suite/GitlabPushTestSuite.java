package com.clarolab.pipeline.suite;

import com.clarolab.pipeline.test.GitlabPushPipelineResultTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        GitlabPushPipelineResultTest.class
})
public class GitlabPushTestSuite {
}
